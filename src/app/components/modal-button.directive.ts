import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[modalButton]',
  exportAs: 'modalButton'
})
export class ModalButtonDirective {

  //@HostListener('click',['$event'])
  click(e){
    this.clicks.emit(e)
  }

  clicks = new EventEmitter()

  constructor() { }

}
