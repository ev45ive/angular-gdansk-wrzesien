import { ModalButtonDirective } from './modal-button.directive';
import { Attribute, Component, ContentChild, Input, OnInit } from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styles: []
})
export class ModalComponent implements OnInit {

  @Input()
  show = false

  @ContentChild(ModalButtonDirective,{read:ModalButtonDirective})
  closeRef:ModalButtonDirective

  ngAfterContentInit(){

  }

  @Input()
  title = ''

  close(){

  }

  constructor(@Attribute('title') title) { }

  ngOnInit() {
    console.log(this.closeRef)
    this.closeRef.clicks.subscribe( click => {
      console.log(click)
    })
  }

}
