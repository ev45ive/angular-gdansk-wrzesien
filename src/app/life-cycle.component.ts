import {
    AfterContentChecked,
    AfterContentInit,
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ApplicationRef,
    Component,
    DoCheck,
    Input,
    NgZone,
    OnChanges,
    OnDestroy,
    OnInit,
} from '@angular/core';

@Component({
  selector: 'life-cycle',
  template: `
    <p #ref>
      life-cycle Works! {{value.message}}
    </p>
    <input [(ngModel)]="value.message">
    <hr/>
    {{counter}}
    <ng-content></ng-content>
  `,
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LifeCycleComponent implements OnInit,
OnChanges,
DoCheck,
AfterContentInit,
AfterContentChecked,
AfterViewInit,
AfterViewChecked,
OnDestroy {

  @Input('value')
  value

  counter = 0;

  constructor(private cdr:ChangeDetectorRef, 
    private zone:NgZone,
    private appRef:ApplicationRef) {
    // this.cdr.detach()
    // setInterval(()=>{
    //   this.cdr.detectChanges()
    // },2000)

    
    this.zone.runOutsideAngular(()=>{
      
      setInterval(()=>{
        this.counter++
        
        if(this.counter > 100){
          this.appRef.tick()
//          this.zone.run(()=>{})
        }

      },100)
    })


    console.log('constructor')
  }
  ngOnInit(...args){
    console.log('ngOnInit', ...args)
  }
  ngOnChanges(...args){
    console.log('ngOnChanges', ...args)
  }
  ngDoCheck(...args){
    console.log('ngDoCheck', ...args)
  }
  ngAfterContentInit(...args){
    console.log('ngAfterContentInit', ...args)
  }
  ngAfterContentChecked(...args){
    console.log('ngAfterContentChecked', ...args)
  }
  ngAfterViewInit(...args){
    console.log('ngAfterViewInit', ...args)
  }
  ngAfterViewChecked(...args){
    console.log('ngAfterViewChecked', ...args)
  }
  ngOnDestroy(...args){
    console.log('ngOnDestroy', ...args)
  }
}
