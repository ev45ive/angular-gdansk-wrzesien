import {
    Directive,
    EmbeddedViewRef,
    Input,
    OnChanges,
    SimpleChange,
    TemplateRef,
    ViewContainerRef,
    ViewRef,
} from '@angular/core';

@Directive({
  selector: '[Unless]',
  // providers:[]
})
export class UnlessDirective implements OnChanges {

  private view:ViewRef;

  @Input('Unless')
  set unless(hide){
    if(hide){
      // this.vcr.clear()
      this.view = this.vcr.detach()
      // this.view.detach()
    }else{
      if(this.view){
        this.vcr.insert(this.view)
        //this.view.reattach()
        //this.vcr.move()
      }else{
        let e = this.vcr.createEmbeddedView(this.tpl,{},0)
        // e.detach()
        // e.detectChanges()
      }      
    }
  }

  // When inputs change:
  ngOnChanges(inputs:{key:SimpleChange}){
    // console.log('ngOnChanges',inputs)
  }

  constructor(private tpl:TemplateRef<any>, 
              private vcr: ViewContainerRef) { }

  ngOnInit(){
    
  }

}
