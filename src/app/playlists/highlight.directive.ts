import { NgFor } from '@angular/common/src/directives/ng_for_of';
import { Directive, ElementRef, Host, HostListener, Inject, Input, OnInit, Renderer2, HostBinding } from '@angular/core';

@Directive({
  // <div highlight></div>
  selector: '[highlight]',
  exportAs:'highlight'
})
export class HighlightDirective implements OnInit {

  @Input('highlight')
  color
  
  @HostBinding('style.borderLeftColor')
  get getMeAColor(){
    return this.hover? this.color : 'initial'
  }

  @HostListener('placki')
  @HostListener('mouseenter',['$event.clientX'])
  onEnter(clientX:number){
    this.hover = true;
  }

  @HostListener('mouseleave')
  onLeave(){
    this.hover = false;
  }

  hover = false;

  constructor(private elem:ElementRef,private renderer:Renderer2) { }

  ngOnInit(){
    // console.log('hello', this.color)

    // this.renderer.setStyle(
    //   this.elem.nativeElement,
    //   'borderLeftColor',
    //   this.color
    // )

  }

}


// console.log(HighlightDirective)