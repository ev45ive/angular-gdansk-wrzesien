import { Component, OnInit, Input } from '@angular/core';
import { Playlist } from './playlist'

@Component({
  selector: 'playlist-details',
  template: `

  <ng-container *ngIf="mode == 'show'; then tplShow; else tplEdit"></ng-container>

    <ng-template #tplShow>
      <div class="form-group">
        <label>Name:</label>
        <div class="form-control-static">{{playlist.name}} </div>
      </div>
      <div class="form-group">
        <div class="form-control-static">
        {{playlist.favourite? 'Favourite' : ''}} </div>
      </div>
      <div class="form-group">
        <label>Color:</label>
        <div class="form-control-static" [style.color]="playlist.color">
        {{playlist.color}} </div>
      </div>
      <button class="btn btn-succes" (click)="edit(formTpl)">Edit</button>
    </ng-template>

    <ng-template #tplEdit>
      <div class="form-group">
          <label>Name:</label>
          <input class="form-control" type="text"
                        [(ngModel)]="playlist.name">
      </div>
      <div class="form-check">
          <label class="form-check-label">
          <input class="form-check-input" type="checkbox" 
                          [(ngModel)]="playlist.favourite">
          Favourite</label>
      </div>
      <div class="form-group">
          <label>Color:</label>
          <input type="color" 
                    [(ngModel)]="playlist.color">
      </div>
      <button class="btn btn-succes" (click)="save()">Save</button>
    </ng-template>

  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist:Playlist

  mode:('show'|'edit') = 'show' 

  save(){
    this.mode = 'show'
  }

  edit(tpl){
    console.log(tpl)
    this.mode = 'edit'
  }

  constructor() { }

  ngOnInit() {
  }

}
