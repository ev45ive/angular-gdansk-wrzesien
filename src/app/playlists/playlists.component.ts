import { Component, OnInit } from '@angular/core';
import { Playlist } from './playlist'

@Component({
  selector: 'playlists',
  template: `
  <div class="row">
    <div class="col">
      <playlists-list [playlists]="playlists"
                      [(selected)]="selected"></playlists-list>
    </div>
    <div class="col">
      <playlist-details 
        *ngIf="selected; else message" 
        [playlist]="selected"></playlist-details>

        <ng-template #message>Please select a playlist</ng-template>
    </div>
  </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {
  
  //counter = 0;

  selected:Playlist;

  playlists: Playlist[] = [
    {
      id: 1,
      name: 'Angular greatest Hits',
      favourite: false,
      color: '#FF0000'
    },
    {
      id: 2,
      name: 'Best of Angular',
      favourite: true,
      color: '#00FF00'
    },
    {
      id: 3,
      name: 'Angular TOP20',
      favourite: false,
      color: '#0000FF'
    }
  ]

  constructor() { }

  ngOnInit() {
    // setInterval(()=>{
    //   this.counter++
    // },500)
  }

}
