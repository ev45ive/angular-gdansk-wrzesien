import { Component, OnInit,ViewEncapsulation,Input,EventEmitter,Output } from '@angular/core';
import { Playlist } from './playlist'

type Playlists = Array<Playlist>|[Playlist]|Playlist[];

// *ngFor="let row of rows; let i = index"
// *ngFor="let col of row.cols; let j = index"
//     {{row}} {{col}}

// [style.borderLeftColor]="( 
//   hover == playlist? playlist.color: 'inherit' 
// )"

// (mouseenter)="hover = playlist"
// (mouseleave)="hover = false"


@Component({
  selector: 'playlists-list',
  template: `
   <div class="list-group">
      <div class="list-group-item"
          [class.active]=" playlist == selected "
         
          [highlight]="playlist.color"
         
          (click)="select(playlist)"
          *ngFor="let playlist of playlists; 
                  let i = index; 
                  trackBy:playlist?.id">
        {{i+1+'.'}} {{playlist.name}}
      </div>
    </div>
  `,
  styles:[`
    .list-group-item{
      border-left: 15px solid black;
    }
  `]
  // inputs:[
  //   "playlists:playlists"
  // ]
})
export class PlaylistsListComponent implements OnInit {

  @Input()
  selected;

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist){
    this.selectedChange.emit(playlist)
  }

  @Input()
  playlists:Playlist[] = []

  constructor() { }

  ngOnInit() {
    // this.select(this.playlists[0])
  }

}
